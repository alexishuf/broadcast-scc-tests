set datafile separator ","
set key inside left top vertical Right noreverse noenhanced autotitle nobox
set style data lines

set style histogram clustered gap 1 title textcolor lt -1
set style data histograms
set xtics border in scale 0,0 nomirror  autojustify
set xtics  norangelimit
set xtics   ()
set ylabel "Selection time (milliseconds)"
set xlabel "Bridges"
set boxwidth 0.2 absolute
set xrange [0 to 6]
set logscale y


plot "scc-rs-exp-master.dat" u (column("bridges")):(column("MEDIATING-STUB-1.composition")-column("MEDIATING-STUB-1.compositionStdDev"))\
                                                   :(column("MEDIATING-STUB-1.compositionMin"))\
                                                   :(column("MEDIATING-STUB-1.compositionMax"))\
                                                   :(column("MEDIATING-STUB-1.composition")+column("MEDIATING-STUB-1.compositionStdDev"))\
                                                     title "1 target" with candlesticks lt rgb '#b07133' fs solid whiskerbars, \
 