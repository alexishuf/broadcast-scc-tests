set datafile separator ","
set key inside left top vertical Right noreverse noenhanced autotitle nobox
set style data lines

set style histogram clustered gap 1 title textcolor lt -1
set style data histograms
set xtics border in scale 0,0 nomirror  autojustify
set xtics  norangelimit
set xtics   ()
set ylabel "Selection time (milliseconds)"
set xlabel "Bridges"
set boxwidth 0.1 absolute
set xrange [0 to 8]
#velho: #12384f
#novo: #174866


plot "scc-rs-master.dat" u (column("bridges")):(column("MEDIATING-STUB-1.composition")-column("MEDIATING-STUB-1.compositionStdDev"))\
                                               :(column("MEDIATING-STUB-1.compositionMin"))\
                                               :(column("MEDIATING-STUB-1.compositionMax"))\
                                               :(column("MEDIATING-STUB-1.composition")+column("MEDIATING-STUB-1.compositionStdDev"))\
                                                title "1 target" with candlesticks lt rgb '#b07133' fs solid whiskerbars, \
"scc-rs-master.dat" u (column("bridges")+0.2):(column("MEDIATING-STUB-2.composition")-column("MEDIATING-STUB-2.compositionStdDev"))\
                                               :(column("MEDIATING-STUB-2.compositionMin"))\
                                               :(column("MEDIATING-STUB-2.compositionMax"))\
                                               :(column("MEDIATING-STUB-2.composition")+column("MEDIATING-STUB-2.compositionStdDev"))\
                                               title "2 targets" with candlesticks lt rgb '#d7191c' fs solid whiskerbars, \
"scc-rs-master.dat" u (column("bridges")+0.4):(column("MEDIATING-STUB-3.composition")-column("MEDIATING-STUB-3.compositionStdDev"))\
                                               :(column("MEDIATING-STUB-3.compositionMin"))\
                                               :(column("MEDIATING-STUB-3.compositionMax"))\
                                               :(column("MEDIATING-STUB-3.composition")+column("MEDIATING-STUB-3.compositionStdDev"))\
                                               title "3 targets" with candlesticks lt rgb '#174866' fs solid whiskerbars, \
"scc-rs-master.dat" u (column("bridges")+0.6):(column("MEDIATING-STUB-4.composition")-column("MEDIATING-STUB-4.compositionStdDev"))\
                                               :(column("MEDIATING-STUB-4.compositionMin"))\
                                               :(column("MEDIATING-STUB-4.compositionMax"))\
                                               :(column("MEDIATING-STUB-4.composition")+column("MEDIATING-STUB-4.compositionStdDev"))\
                                               title "4 targets" with candlesticks lt rgb '#000000' fs solid whiskerbars