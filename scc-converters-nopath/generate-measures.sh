#!/bin/bash

DIR=scc-converters-nopath
GENERATIONS=${GENERATIONS:-1}
ITERATIONS=${ITERATIONS:-16}
PROTOCOLS=${PROTOCOLS:-16}
SVCS=${SVCS:-45}

for (( generation=1; generation <= $GENERATIONS; generation++ )); do
    echo "Freezing generation $generation/$GENERATIONS"
    rm -fr $DIR/gen-$generation &> /dev/null
    mkdir $DIR/gen-$generation
    ./run.sh --simple-converter-path --simple-converters-nopath --inputs 1 --outputs 1 --forced-path-length 0 --type-families-size 3 --simple-converters-types $((SVCS*2)) --simple-converters-choices-max $PROTOCOLS --simple-converters-protocols-min $PROTOCOLS --simple-converters-protocols-max $PROTOCOLS --freeze $DIR/gen-$generation  --max-samples $(($PROTOCOLS-1))
    cp $DIR/gen-$generation/measures /tmp/unshuffled_measures
    ./run.sh --load-measures /tmp/unshuffled_measures --save-measures $DIR/gen-$generation/measures --save-measures-shuffle --max-samples $PROTOCOLS
    cp -r $DIR/gen-tpl/* $DIR/gen-$generation/
    sed s/gen-tpl/gen-$generation/g -i $DIR/gen-$generation/aggr-master.ktr
done
