#!/bin/bash

for i in {1..5}; do 
    cp gen-tpl/aggr-master.ktr gen-$i/aggr-master.ktr 
    sed s/gen-tpl/gen-$i/g -i gen-$i/aggr-master.ktr
    pan.sh -file $(pwd)/gen-$i/aggr-master.ktr
    cp gen-$i/master-aggr.data aggrs/master-aggr.gen-$i.data
done
pan.sh -file $(pwd)/aggr-aggrs.ktr 
