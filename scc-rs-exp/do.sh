#!/bin/bash

DIR=scc-rs-exp
GENERATIONS=${GENERATIONS:-1}
ITERATIONS=${ITERATIONS:-8}
PAN=${PAN:-pan.sh}

function clear_stardog() {
    rm -fr ~/apps/stardog-data/* && cp ~/apps/stardog4-license-key.bin ~/apps/stardog-data/stardog-license-key.bin
}

function fuck_java() {
    for i in {1..3}; do
        (killall java 2>&1) >  /dev/null
        sleep 3s
    done
    for i in {1..3}; do
        (killall -9 java 2>&1) >  /dev/null
        sleep 5s
    done
}

function wait_for() {
    for ((i=0; i < $2; ++i)); do
        sleep 5s;
        if ! [ -d /proc/$1 ]; then
            return 0
        fi
    done
    kill $1
    sleep 5s;
    if [ -d /proc/$1 ]; then
        kill -9 $1
        sleep 5s;
    fi
    return 1
}

function measure_count() {
    ./run.sh --load-measures $1  --dump-measures | wc -l 
}

#
# master
#
git checkout master || exit 1
git rev-parse --short HEAD > $DIR/master.commit
../gradlew clean
../gradlew classes testClasses || exit 1
for ((generation=1; generation<=$GENERATIONS; generation++)); do
    GENDIR=$DIR/gen-$generation
    for ((iteration=1; iteration<=$ITERATIONS; iteration++)); do
        clear_stardog.sh
        cp $GENDIR/measures $GENDIR/eat_measures
        rm $GENDIR/master.{data,log}$iteration
        while [ "$(measure_count $GENDIR/eat_measures)" != "0" ]; do
            unset OK
            fuck_java
            $DIR/do_run.sh $GENDIR $iteration master & 
            PID=$!
            wait_for $PID 360 && OK=1
            echo -e "\n\n"
            if [ -e $OK ]; then
                echo -e "WARNING: do_run HANGED"
            fi
            MEASURES_LEFT=$(measure_count $GENDIR/eat_measures)
            echo "generation=$generation, iteration=$iteration, measures left=$MEASURES_LEFT"
            #~ sleep 5s
        done
        $PAN -file $(pwd)/$GENDIR/aggr-master.ktr
	sed s/_/-/g -i $GENDIR/master-aggr.data
    done
done


