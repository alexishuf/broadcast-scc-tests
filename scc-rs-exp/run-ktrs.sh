#!/bin/bash

for i in {1..1}; do 
    for action in aggr stats; do
        cp gen-tpl/$action-master.ktr gen-$i/$action-master.ktr 
        sed s/gen-tpl/gen-$i/g -i gen-$i/$action-master.ktr
        pan.sh -file $(pwd)/gen-$i/$action-master.ktr
        sed s/_/-/g -i gen-$i/master-$action.data 
    done
done
#pan.sh -file $(pwd)/$action-$actions.ktr 
