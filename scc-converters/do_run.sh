#!/bin/bash
# do_run.sh dir iteration basename
./run.sh --eat-measures $1/eat_measures --output $1/$3.data$2 --max-samples 4 2>&1 | tee -a $1/$3.log$2
