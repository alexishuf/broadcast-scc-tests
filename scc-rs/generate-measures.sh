#!/bin/bash

DIR=scc-rs
GENERATIONS=${GENERATIONS:-1}
ITERATIONS=${ITERATIONS:-16}
PROTOCOLS=${PROTOCOLS:-8}
ADMISSIBLE_BRIDGES=${ADMISSIBLE_BRIDGES:-5}
SVCS=${SVCS:-45}

for (( generation=1; generation <= $GENERATIONS; generation++ )); do
    echo "Freezing generation $generation/$GENERATIONS"
    rm -fr $DIR/gen-$generation &> /dev/null
    mkdir $DIR/gen-$generation
    ./run.sh --random-studio --inputs 1 --outputs 1  --rs-pair-profiles --rs-min-protocols $PROTOCOLS --rs-max-protocols $PROTOCOLS --rs-min-services $SVCS --rs-max-services $SVCS --rs-services-step 0 --rs-min-types $((SVCS*2)) --rs-max-types $((SVCS*2)) --rs-types-step 0 --rs-result-type-selections 1 --child-Xms 800M --child-Xmx 1400M --max-samples $PROTOCOLS --freeze $DIR/gen-$generation
    mv $DIR/gen-$generation/measures /tmp/unshuffled_measures
    ./run.sh --load-measures /tmp/unshuffled_measures --save-measures $DIR/gen-$generation/admissible_measures --save-measures-shuffle --max-samples $ADMISSIBLE_BRIDGES
    ./run.sh --load-measures /tmp/unshuffled_measures --save-measures $DIR/gen-$generation/measures --save-measures-shuffle --max-samples $PROTOCOLS
    cp -r $DIR/gen-tpl/* $DIR/gen-$generation/
    sed s/gen-tpl/gen-$generation/g -i $DIR/gen-$generation/aggr-master.ktr
    sed s/gen-tpl/gen-$generation/g -i $DIR/gen-$generation/aggr-admissible_heuristic.ktr
done
