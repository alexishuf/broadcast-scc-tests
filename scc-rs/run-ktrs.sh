#!/bin/bash
GENERATIONS=${GENERATIONS:-$1}
for (( i=1; i<=$GENERATIONS; i++ )); do
    for action in aggr stats; do
        cp gen-tpl/$action-master.ktr gen-$i/$action-master.ktr 
        sed s/gen-tpl/gen-$i/g -i gen-$i/$action-master.ktr
        pan.sh -file $(pwd)/gen-$i/$action-master.ktr
        sed s/_/-/g -i gen-$i/master-$action.data 
        
        cp gen-tpl/$action-admissible_heuristic.ktr gen-$i/$action-admissible_heuristic.ktr 
        sed s/gen-tpl/gen-$i/g -i gen-$i/$action-admissible_heuristic.ktr
        pan.sh -file $(pwd)/gen-$i/$action-admissible_heuristic.ktr
        sed s/_/-/g -i gen-$i/admissible_heuristic-$action.data 
    done
done
